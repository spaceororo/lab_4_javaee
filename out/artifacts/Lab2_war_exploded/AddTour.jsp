<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>Add new tour</title>
</head>
<body>

<h1>Add Tour</h1>
<c:if test="${not empty error}">
    <br>
    <p style="color: red;">${error}</p>
    <br>
</c:if>
<form action="AddTour" method="POST">
    <%--<p>Code</p>
    <input name="code" value="${tour.idTour}" required oninvalid="this.setCustomValidity('Fill this field!')"
           oninput="setCustomValidity('')"/> <br>--%>
    <p>Name</p>
    <input name="name_tour" value="${tour.nameTour}" required oninvalid="this.setCustomValidity('Fill this field!')"
           oninput="setCustomValidity('')"/> <br>
    <p>Duration:</p>
    <p><input name="duration" pattern="^[ 0-9]+$" required oninvalid="this.setCustomValidity('Fill this field!')"
             oninput="setCustomValidity('')" value="${tour.duration}"><br>
    </p> <br>
    <p>Description</p>
    <p><textarea rows="10" cols="45" name="description" required oninvalid="this.setCustomValidity('Fill this field!')"
                 oninput="setCustomValidity('')">${tour.description}</textarea></p> <br>

        <%--<p>Type transport:${tour.transport.name_transport}</p>--%>
        <%-- <select required>
             <option disabled>Shange type transport</option>--%>
        <table>
            <tr>
                <th>Type Transport</th>
                <th>Action</th>
            </tr>
            <c:forEach items="${requestScope.listTransport}" var="transport">
                <%-- <option name="id_transport" value="${transport.id_transport}"><c:out value="${transport.name_transport}"></c:out></option>--%>
                <tr>
                    <th>
                        <c:out value="${transport.name_transport}"></c:out>
                    </th>
                    <th>
                        <input type="radio" name="id_transport" value="${transport.id_transport}">
                    </th>
                </tr>
            </c:forEach>
        </table>
    <%--<input name="id_transport" value="${tour.transport.id_transport}"><br>--%>
        <input type="submit" value="Save"> <br>
</form>
</body>
</html>