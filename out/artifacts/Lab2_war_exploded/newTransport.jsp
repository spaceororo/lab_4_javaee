<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>Add new tour</title>
</head>
<body>

<h1>Add Tour</h1>
<c:if test="${not empty error}">
    <br>
    <p style="color: red;">${error}</p>
    <br>
</c:if>
<form action="AddTransport" method="POST">
    <%--<p>Code</p>
    <input name="code" value="${tour.idTour}" required oninvalid="this.setCustomValidity('Fill this field!')"
           oninput="setCustomValidity('')"/> <br>--%>
    <p>Name</p>
    <input name="name_transport" value="${tour.nameTour}" required oninvalid="this.setCustomValidity('Fill this field!')"
           oninput="setCustomValidity('')"/> <br>

    <input type="submit" value="Save"> <br>
</form>
</body>
</html>