<%@ page language="java" contentType="text/html; charset=US-ASCII"
         pageEncoding="US-ASCII" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
    <title>Home Page</title>

    <style>

        table, th, td {
            border: 1px solid black;
        }

        .layer {
            overflow-x: hidden;
            max-width: 400px;
            height: 150px;
        }

        #line_block {
            width: 110px;
            height: 50px;
            float: left;
            margin: 0 15px 15px 0;
            text-align: center;
            padding: 10px;
        }

    </style>
</head>
<body>
<jsp:include page="_header.jsp"></jsp:include>
<p>
<div id="line_block">
    <form action="AddTour">
        <input style="width: 85px" class="btn btn-outline-success" type="submit" value="New Tour"/>
    </form>
</div>
<div id="line_block">
    <form action="Save">
        <input style="width: 85px" class="btn btn-outline-primary" type="submit" value="Save"
               onclick="return confirm('Do you want to save record?')"/>
    </form>
</div>
<div id="line_block">
    <form action="Open">
        <input style="width: 85px" class="btn btn-outline-secondary" type="submit" value="Open"/>
    </form>
</div>
</p>

<table class="table" style="float:left; width: 750px" >
    <tbody>
    <tr>
        <th>Tour code</th>
        <th>Tour name</th>
        <th>Transport</th>
        <th>Duration</th>
        <th>Tour description</th>
        <th>Action</th>
    </tr>
    <c:forEach items="${requestScope.tourList}" var="tour">
        <tr>
            <td ><c:out value="${tour.id_tour}"></c:out></td>
            <td><c:out value="${tour.name_tour}"></c:out></td>
            <td><c:out value="${tour.transport.name_transport}"></c:out></td>
            <td><c:out value="${tour.duration}"></c:out> day(s)</td>
            <td width="400">
                <div class="layer">
                    <c:out value="${tour.description}"></c:out>
                </div>
            <td>
                <form method="POST" action="DeleteTour">
                    <input hidden name="id_tour" value="${tour.id_tour}"/>
                    <input type="submit" style="width: 75px" class="btn btn-danger" value="Delete"
                           onclick="return confirm('Do you want delete this record?')">
                </form>
                <br>
                <form method="GET" action="EditTour">
                    <input hidden name="id_tour" value="${tour.id_tour}"/>
                    <input hidden name="name_tour" value="${tour.name_tour}"/>
                    <input hidden name="name_transport" value="${tour.transport.name_transport}"/>
                    <input hidden name="id_transport" value="${tour.transport.id_transport}"/>
                    <input hidden name="duration" value="${tour.duration}"/>
                    <input hidden name="description" value="${tour.description}"/>
                    <input type="submit" style="width: 75px" class="btn btn-warning" value="Edit">
                </form>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div id="line_block">
<form action="Search" method="POST">
    <input type="text" name="search" pattern="^[ 0-9]+$"
           required oninvalid="this.setCustomValidity('Fill this field or check input!')" oninput="setCustomValidity('')">
    <input type="submit" value="Search by tour code" class="btn btn-outline-primary">

</form>
    <br>
    <form action="Sort" method="post">
        <input type="submit" value="Sort descending tour code" class="btn btn-outline-primary">
    </form>
</div>
</body>
</html>
