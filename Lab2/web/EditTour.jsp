<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>Edit tour</title>
</head>
<body>

<h1>Edit Tour</h1>

<form action="EditTour" method="POST">
    <p>Code: ${tour.id_tour}</p>
    <input type="hidden" name="id_tour" value="${tour.id_tour}">
    <p>Name:</p>
    <input name="name_tour" value="${tour.name_tour}" required oninvalid="this.setCustomValidity('Fill this field!')"
           oninput="setCustomValidity('')"/> <br>
    <p>Duration:</p>
    <p><input name="duration" required oninvalid="this.setCustomValidity('Fill this field!')"
              oninput="setCustomValidity('')" value="${tour.transport.id_transport}"><br>
    </p> <br>
    <p>Description:</p>
    <p><textarea rows="10" cols="40" name="description" required oninvalid="this.setCustomValidity('Fill this field!')"
                 oninput="setCustomValidity('')">${tour.description}</textarea></p> <br>
    <p>Type transport: ${tour.transport.name_transport}</p>
    <%-- <select required>
         <option disabled>Shange type transport</option>--%>
    <table>
        <tr>
            <th>Type Transport</th>
            <th>Action</th>
          <%--  <th>
                <form action="Change" method="post" >
                    <input type="submit" name="change" value="Change type transport">
                </form>
            </th>--%>
        </tr>
        <c:forEach items="${requestScope.listTransport}" var="transport">
            <%-- <option name="id_transport" value="${transport.id_transport}"><c:out value="${transport.name_transport}"></c:out></option>--%>
            <tr>
                <th>
                    <c:out value="${transport.name_transport}"></c:out>
                </th>
                <th>
                    <input type="radio" name="id_transport" value="${transport.id_transport}">
                    <input hidden name="name_transport" value="${transport.name_transport}">
                </th>
            </tr>
        </c:forEach>
    </table>
    <%--</select>--%>
    <%-- <input name="id_transport" value="${tour.transport.id_transport}"><br>--%>
    <input type="hidden" name="name_transport" value="${tour.transport.name_transport}">
    <input type="submit" value="Save"> <br>
    <%--<c:forEach items="${requestScope.listTransport}" var="transport">
        <option name="id_transport" value="${transport.id_transport}"><c:out value="${transport.name_transport}"></c:out></option>
    </c:forEach>--%>
</form>
</body>
</html>