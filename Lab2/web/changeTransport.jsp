<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>Edit transport</title>
</head>
<body>

<h1>Edit Tour</h1>

<form action="Change" method="POST">

    <table border="1">
        <tr>
            <th>Type Transport</th>
            <th>Action</th>
        </tr>
        <c:forEach items="${requestScope.listTransport}" var="transport">
            <tr>
                <th>
                    <c:out value="${transport.name_transport}"></c:out>
                </th>
                <th>
                    <form method="POST" action="DeleteTransport">
                        <input hidden name="id_transport" value="${transport.id_transport}"/>
                        <input hidden name="name_transport" value="${transport.name_transport}"/>
                        <input type="submit" style="width: 75px" class="btn btn-danger" value="Delete"
                               onclick="return confirm('Do you want delete this record?')">
                    </form>
                    <br>
                    <form method="GET" action="EditTransport">
                        <input hidden name="id_transport" value="${transport.id_transport}"/>
                        <input hidden name="name_transport" value="${transport.name_transport}"/>
                        <input type="submit" style="width: 75px" class="btn btn-warning" value="Edit">
                    </form>
                </th>
            </tr>
        </c:forEach>
    </table>
    <input type="hidden" name="name_transport" value="${tour.transport.name_transport}">
    <input type="submit" value="Save"> <br>
</form>
<form action="PrintTour">
    <input type="submit" value="Back">
</form>
</body>
</html>