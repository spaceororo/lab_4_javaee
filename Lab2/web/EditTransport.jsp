<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>Edit transport</title>
</head>
<body>

<h1>Edit Transport</h1>

<form action="EditTransport" method="POST">
    <p>Name:</p>
    <input name="name_transport" value="${transport.name_transport}" required oninvalid="this.setCustomValidity('Fill this field!')"

           oninput="setCustomValidity('')"/> <br>
<input hidden name="id_transport" value="${transport.id_transport}"></input>
    <%-- <input name="id_transport" value="${tour.transport.id_transport}"><br>--%>

    <input type="submit" value="Save"> <br>
    <%--<c:forEach items="${requestScope.listTransport}" var="transport">
        <option name="id_transport" value="${transport.id_transport}"><c:out value="${transport.name_transport}"></c:out></option>
    </c:forEach>--%>
</form>
<form action="newTransport.jsp">
    <input type="submit" value="Add Transport">
</form>
</body>
</html>