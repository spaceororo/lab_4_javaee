package servlets;

import model.CollectionTour;
import model.DBcmd;
import model.MySqlConn;
import model.Tour;
import org.w3c.dom.ls.LSOutput;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sound.sampled.Line;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashSet;

@WebServlet("/Save")
public class SaveServlet extends HttpServlet {
    public SaveServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = MySqlConn.MyConn();
            System.out.println("Connected in Save...");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        LinkedHashSet<Tour> listTour = null;
        try {
            listTour = DBcmd.queryTour(conn);
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        FileOutputStream fos1 = new FileOutputStream("d:\\ser.xml");
        XMLEncoder xe1 = new XMLEncoder(fos1);
        xe1.writeObject(listTour);
        xe1.close();
        response.sendRedirect("PrintTour");
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
