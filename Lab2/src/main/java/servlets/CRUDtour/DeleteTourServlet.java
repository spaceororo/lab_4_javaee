package servlets.CRUDtour;

import model.CollectionTour;
import model.DBcmd;
import model.MySqlConn;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet("/DeleteTour")
public class DeleteTourServlet extends HttpServlet {
    public DeleteTourServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection conn = null;
        int id_tour = Integer.parseInt(request.getParameter("id_tour"));
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = MySqlConn.MyConn();
            System.out.println("Connected in Add...");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        try {
            if (DBcmd.queryDeleteTour(conn, Integer.toString(id_tour))) {
                conn.close();
                response.sendRedirect("PrintTour");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
