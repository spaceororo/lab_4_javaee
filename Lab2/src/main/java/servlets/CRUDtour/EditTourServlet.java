package servlets.CRUDtour;

import model.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedHashSet;

@WebServlet("/EditTour")
public class EditTourServlet extends HttpServlet {
    public EditTourServlet() {
        super();
    }


    protected void doPost(HttpServletRequest rs, HttpServletResponse response) throws ServletException, IOException {
        int id_tour = Integer.parseInt(rs.getParameter("id_tour"));
        String name_tour = rs.getParameter("name_tour");
        String name_transport = rs.getParameter("name_transport");
        int id_transport = Integer.parseInt(rs.getParameter("id_transport"));
        String duration = rs.getParameter("duration");
        String description = rs.getParameter("description");
        System.out.println("--------------->" + name_transport);
        Tour newTour = new Tour(id_tour, name_tour, new Transport(id_transport, name_transport), duration, description);

        Connection conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = MySqlConn.MyConn();
            System.out.println("Connected in Add...");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            System.out.println("----------ERROR----------!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }

        try {
            if (DBcmd.queryUpdateTour(conn, newTour)) {
                newTour = null;
                conn.close();
                response.sendRedirect("PrintTour");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Tour tour = new Tour(Integer.parseInt(request.getParameter("id_tour")),
                (String) request.getParameter("name_tour"),
                new Transport(Integer.parseInt(request.getParameter("id_transport")),
                        (String) request.getParameter("name_transport")),
                (String) request.getParameter("duration"), (String) request.getParameter("description"));
        request.setAttribute("tour", tour);
        //CollectionTour.delTour(tour.getIdTour());

        Connection conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = MySqlConn.getMySqlConn("localhost", "spacetour", "myuser", "12345");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            System.out.println("----------ERROR----------!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }

        LinkedHashSet<Transport> listTransport = null;
        try {
            listTransport = DBcmd.listTransport(conn);
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        request.setAttribute("listTransport", listTransport);
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/EditTour.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
