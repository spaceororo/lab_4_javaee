package servlets.CRUDtransport;

import model.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashSet;

@WebServlet("/AddTransport")
public class AddTransportServlet extends HttpServlet {
    public AddTransportServlet() {
        super();
    }

    public static int idTour = CollectionTour.getList().getidlasttour();


    protected void doPost(HttpServletRequest rs, HttpServletResponse response) throws ServletException, IOException {
        String name_transport = rs.getParameter("name_transport");

        Connection conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = MySqlConn.MyConn();
            System.out.println("Connected in Add...");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        idTour += 1;
        System.out.println("idTour    ------>" + idTour);
        String errorText = null;
        //Tour addTour = new Tour(name, desc);
        Transport addTransport = new Transport(1,name_transport);
        try {
            if (DBcmd.queryInsertTransport(conn, addTransport)) {
                addTransport = null;
                conn.close();
                response.sendRedirect("PrintTour");
            } else {
                errorText = "Data has mistakes!!! Try again...";
                System.out.println("Data has mistakes!!! Try again");
                rs.setAttribute("tour", addTransport);
                rs.setAttribute("error", errorText);
                RequestDispatcher dispatcher = rs.getServletContext().getRequestDispatcher("/AddTour.jsp");
                dispatcher.forward(rs, response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("tourList in AddTour finish..." + rs.getAttribute("tourList"));
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = MySqlConn.getMySqlConn("localhost", "spacetour", "myuser", "12345");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            System.out.println("----------ERROR----------!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }

        LinkedHashSet<Transport> listTransport = null;
        try {
            listTransport = DBcmd.listTransport(conn);
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.setAttribute("listTransport", listTransport);
        System.out.println("tourList in AddTout start..." + request.getAttribute(CollectionTour.getList().toString()));
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/AddTransport.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
