package servlets.CRUDtransport;

import model.CollectionTour;
import model.DBcmd;
import model.MySqlConn;
import model.Transport;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet("/DeleteTransport")
public class DeleteTransportServlet extends HttpServlet {
    public DeleteTransportServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection conn = null;
        int id_transport = Integer.parseInt(request.getParameter("id_transport"));
        String name_transport = request.getParameter("name_transport");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = MySqlConn.MyConn();
            System.out.println("Connected in Add...");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        try {
            if (DBcmd.queryDeleteTrasport(conn, new Transport(id_transport,name_transport))) {
                conn.close();
                response.sendRedirect("PrintTour");
            }else{response.sendRedirect("PrintTour");}
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
