package servlets.CRUDtransport;

import model.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedHashSet;

@WebServlet("/EditTransport")
public class EditTransportServlet extends HttpServlet {
    public EditTransportServlet() {
        super();
    }


    protected void doPost(HttpServletRequest rs, HttpServletResponse response) throws ServletException, IOException {
        int id_transport = Integer.parseInt(rs.getParameter("id_transport"));
        String name_transport = rs.getParameter("name_transport");

        System.out.println("--------------->" + name_transport);
        Transport newTransport = new Transport(id_transport, name_transport);
System.out.println("123456543223456765432 "+newTransport.toString());
        Connection conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = MySqlConn.MyConn();
            System.out.println("Connected in Add...");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            System.out.println("----------ERROR----------!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }

        try {
            if (DBcmd.queryUpdateTransport(conn, newTransport)) {
                newTransport = null;
                conn.close();
                response.sendRedirect("Change");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Transport transport = new Transport(Integer.parseInt(request.getParameter("id_transport")),
                (String) request.getParameter("name_transport"));
        request.setAttribute("transport", transport);
        //CollectionTour.delTour(tour.getIdTour());

        Connection conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = MySqlConn.getMySqlConn("localhost", "spacetour", "myuser", "12345");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        LinkedHashSet<Transport> listTransport = null;
        try {
            listTransport = DBcmd.listTransport(conn);
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        request.setAttribute("listTransport", listTransport);
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/EditTransport.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
