package servlets.CRUDtransport;

import model.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashSet;

@WebServlet("/Change")
public class ChangeTransportServlet extends HttpServlet {
    public ChangeTransportServlet() {
        super();
    }



    protected void doPost(HttpServletRequest rs, HttpServletResponse response) throws ServletException, IOException {
        doGet(rs, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = MySqlConn.MyConn();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        LinkedHashSet<Transport> listTransport = null;
        try {
            listTransport = DBcmd.listTransport(conn);
            conn.close();
            System.out.println(listTransport.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("list transport"+listTransport.toString());
        request.setAttribute("listTransport", listTransport);
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/changeTransport.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
