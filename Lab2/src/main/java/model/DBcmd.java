package model;

import javax.sound.midi.Soundbank;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;

public class DBcmd {
    public static LinkedHashSet<Tour> queryTour(Connection conn) throws SQLException {
        String sql = "select tour.*, tour_transport.name_transport from tour_transport, " +
                "tour where tour.id_tour_transport=tour_transport.id_transport order by id_tour";
        PreparedStatement pstm = conn.prepareStatement(sql);
        ResultSet rs = pstm.executeQuery();
        LinkedHashSet tours = new LinkedHashSet();
        while (rs.next()) {
            int id_tour = rs.getInt("id_tour");
            String name_tour = rs.getString("name_tour");
            int id_tour_transport = rs.getInt("id_tour_transport");
            String name_transport = rs.getString("name_transport");
            String duration = rs.getString("duration");
            String description = rs.getString("description");
            Tour tour = new Tour(id_tour, name_tour, new Transport(id_tour_transport,name_transport), duration, description);
            tours.add(tour);
        }
        pstm.close();
        return tours;
    }
    public static LinkedHashSet<Transport> listTransport(Connection conn) throws SQLException{
        LinkedHashSet listTransport= new LinkedHashSet();
        String sql ="select * from tour_transport";
        PreparedStatement pstm=conn.prepareStatement(sql);
        ResultSet rs = pstm.executeQuery();
        while (rs.next()) {
            int id_transport = rs.getInt("id_transport");
            String name_transport = rs.getString("name_transport");
            listTransport.add(new Transport(id_transport,name_transport));
        }
        System.out.println("list transport in DBcom"+listTransport.toString());
        return listTransport;
    }


    public static LinkedHashSet<Tour> queryFindTourById(Connection conn, String id_tour_search) throws SQLException {
        String sql = "select tour.*, tour_transport.name_transport from tour_transport, " +
                "tour where tour.id_tour_transport=tour_transport.id_transport and id_tour=?";

        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, id_tour_search);

        ResultSet rs = pstm.executeQuery();
        LinkedHashSet tours = new LinkedHashSet();
        while (rs.next()) {
            int id_tour = rs.getInt("id_tour");
            String name_tour = rs.getString("name_tour");
            int id_tour_transport = rs.getInt("id_tour_transport");
            String name_transport = rs.getString("name_transport");
            String duration = rs.getString("duration");
            String description = rs.getString("description");
            Tour tour = new Tour(id_tour, name_tour, new Transport(id_tour_transport,name_transport), duration, description);
            tours.add(tour);
        }
        pstm.close();
        return tours;
    }
/*
    public static LinkedHashSet queryFindTourByName(Connection conn, String name_tour_search) throws SQLException {
        String sql = "Select * from tours where name_tour=?";

        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, name_tour_search);
        ResultSet rs = pstm.executeQuery();
        LinkedHashSet tours = new LinkedHashSet();
        while (rs.next()) {
            int id_tour = rs.getInt("id_tour");
            String name_tour = rs.getString("name_tour");
            String desc_tour = rs.getString("desc_tour");
            Tour tour = new Tour(id_tour, name_tour, desc_tour);
            tours.add(tour);
        }
        return tours;
    }
*/
    public static boolean queryUpdateTour(Connection conn, Tour tour) throws SQLException {
        String sql = "Update tour set name_tour =?, id_tour_transport=?,duration=?, description=? where id_tour=? ";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, tour.getName_tour());
        System.out.println("||||||||||"+tour.getId_tour()+""+tour.getName_tour()+"||||||||||"+tour.getDescription()
                +"||||||||||||||||"+tour.getDuration()+"|||||||||||||||"+tour.getTransport().getName_transport());
        pstm.setInt(2, tour.getTransport().getId_transport());
        pstm.setString(3, tour.getDuration());
        pstm.setString(4, tour.getDescription());
        pstm.setString(5, tour.getId_tour()+"");
        pstm.executeUpdate();
        return true;
    }

    public static boolean queryInsertTour(Connection conn, Tour tour) throws SQLException {
        String sql = "insert into tour(name_tour,id_tour_transport, duration,description) values(?,?,?,?)";
        PreparedStatement pstm = conn.prepareStatement(sql);
        System.out.println("BLA BLA BLA"+tour.toString());
        pstm.setString(1, tour.getName_tour());
        pstm.setInt(2, tour.getTransport().getId_transport());
        pstm.setInt(3, Integer.parseInt(tour.getDuration()));
        pstm.setString(4, tour.getDescription());
        System.out.println("||||||||||"+tour.getId_tour()+""+tour.getName_tour()+"||||||||||"+tour.getDescription()
                +"||||||||||||||||"+tour.getDuration()+"|||||||||||||||"+tour.getTransport().getName_transport()+"|||"+tour.getTransport().getId_transport());
        pstm.executeUpdate();
        return true;
    }

    public static boolean queryDeleteTour(Connection conn, String code) throws SQLException {
        String sql = "Delete From tour where id_tour= ?";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, code);
        pstm.executeUpdate();
        return true;
    }


    public static LinkedHashSet<Tour> sortTour(Connection conn) throws SQLException {
        String sql = "select tour.*, tour_transport.name_transport from tour_transport, " +
                "tour where tour.id_tour_transport=tour_transport.id_transport order by id_tour desc";
        PreparedStatement pstm = conn.prepareStatement(sql);
        ResultSet rs = pstm.executeQuery();
        LinkedHashSet tours = new LinkedHashSet();
        while (rs.next()) {
            int id_tour = rs.getInt("id_tour");
            String name_tour = rs.getString("name_tour");
            int id_tour_transport = rs.getInt("id_tour_transport");
            String name_transport = rs.getString("name_transport");
            String duration = rs.getString("duration");
            String description = rs.getString("description");
            Tour tour = new Tour(id_tour, name_tour, new Transport(id_tour_transport,name_transport), duration, description);
            tours.add(tour);
        }
        pstm.close();
        return tours;
    }

    public static boolean queryUpdateTransport(Connection conn, Transport newTransport) throws SQLException {
        String sql = "Update tour_transport set name_transport =? where id_transport=? ";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, newTransport.getName_transport());
        pstm.setInt(2, newTransport.getId_transport());
        pstm.executeUpdate();
        return true;
    }

    public static boolean queryInsertTransport(Connection conn, Transport addTransport) throws SQLException {
        String sql = "insert into tour_transport(name_transport) values(?)";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, addTransport.getName_transport());
        pstm.executeUpdate();
        return true;
    }

    public static boolean queryDeleteTrasport(Connection conn, Transport transport) throws SQLException {
        String sql ="select * from tour where id_tour_transport=?";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, Integer.toString(transport.getId_transport()));
        ResultSet rs = pstm.executeQuery();
        if (rs.next()){
            return false;
        }else {
            sql="delete from tour_transport where id_transport = ?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, Integer.toString(transport.getId_transport()));
            ps.executeUpdate();
            return true;
        }
    }

}
