package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class MySqlConn {
    public static Connection getMySqlConn()
            throws ClassNotFoundException, SQLException {
        String hostName = "localhost";
        String dbName = "spacetour";
        String userName = "root";
        String password = "12345";
        return getMySqlConn(hostName, dbName, userName, password);
    }

    public static Connection getMySqlConn(String hostName, String dbName,
                                          String userName, String password) throws SQLException,
            ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        //"jdbc:mysql://localhost/store?serverTimezone=Europe/Moscow&useSSL=false"
        String connectionURL = "jdbc:mysql://" + hostName + ":3306/" + dbName +"?serverTimezone=Europe/Moscow&useSSL=false&allowPublicKeyRetrieval=true";
        Connection conn = DriverManager.getConnection(connectionURL, userName,
                password);
        return conn;
    }

    public static Connection MyConn() throws SQLException, ClassNotFoundException {
        return getMySqlConn("localhost", "spacetour", "root", "12345");
    }
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/spacetour", "root",
                    "12345");
            System.out.println("Connected");
            Statement statement = conn.createStatement();
            statement.executeUpdate("insert into tours (name_tour,desc_tour) values ('from Java','from Java')");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
/*
public class MySqlConn {
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/spacetour", "root",
                    "12345");
            System.out.println("Connected");
            Statement statement = conn.createStatement();
            statement.executeUpdate("insert into tours (name_tour,desc_tour) values ('from Java','from Java')");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
*/