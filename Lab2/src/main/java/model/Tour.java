package model;

import java.io.Serializable;
import java.util.Objects;

public class Tour implements Serializable {
    private int id_tour;
    private String name_tour;
    private Transport transport;
    private String duration;
    private String description;

    public Tour(int idTour, String name_tour, Transport transport, String duration, String description) {
        this.id_tour = idTour;
        this.name_tour = name_tour;
        this.transport = transport;
        this.duration=duration;
        this.description = description;
    }

    public Tour(String name_tour, String description) {
        this.name_tour = name_tour;
        this.description = description;
    }

    public Tour() {
    }

    public int getId_tour() {
        return id_tour;
    }

    public void setId_tour(int id_tour) {
        this.id_tour = id_tour;
    }

    public String getName_tour() {
        return name_tour;
    }

    public void setName_tour(String name_tour) {
        this.name_tour = name_tour;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean equals(Tour o) {
        boolean TF = true;
        if (this.name_tour == o.name_tour)
            TF = false;
        return TF;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id_tour, name_tour, description);
    }

    @Override
    public String toString() {
        return "Tour{" +
                "id_tour=" + id_tour +
                ", name_tour='" + name_tour + '\'' +
                ", id_tour_transport=" + transport +
                ", duration='" + duration + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
