package model;

import java.io.Serializable;

public class Transport implements Serializable {
    private int id_transport;
    private String name_transport;

    public Transport(int id_transport, String name_transport) {
        this.id_transport = id_transport;
        this.name_transport = name_transport;
    }

    public int getId_transport() {
        return id_transport;
    }

    public void setId_transport(int id_transport) {
        this.id_transport = id_transport;
    }

    public String getName_transport() {
        return name_transport;
    }

    @Override
    public String toString() {
        return "Transport{" +
                "id_transport=" + id_transport +
                ", name_transport='" + name_transport + '\'' +
                '}';
    }

    public void setName_transport(String name_transport) {
        this.name_transport = name_transport;
    }
}
